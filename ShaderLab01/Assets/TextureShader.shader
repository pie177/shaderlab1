﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextureShader" 
{
	Properties 
	{
		_MainTex("Diffuse Texture", 2D) = "white" {}
		_ColorTint("Color Tint", Color) = (1,1,1,1)
	}
	SubShader 
	{
		Pass 
		{
			CGPROGRAM

			#pragma vertex vertexFunction
			#pragma fragment fragmentFunction

			//user defined variables
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _ColorTint;

			//unity declared variables

			//input struct
			struct inputStruct
			{
				float4 vertexPos : POSITION;
				float4 textureCoord: TEXCOORD0;
			};

			//output struct
			struct outputStruct
			{
				float4 pixelPos : SV_POSITION;
				float4 tex : TEXCOORD0;
			};

			//vertex program
			outputStruct vertexFunction(inputStruct input)
			{
				outputStruct toReturn;

				toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);
				toReturn.tex = input.textureCoord;

				return toReturn;
			}

			//fragment program
			float4 fragmentFunction(outputStruct input) : COLOR
			{
				//original offset and tilting
				//float4 tex = tex2D(_MainTex, _MainTex_ST.xy * input.tex.xy + _MainTex_ST.zw);

				//inverse offset and tilting
				float4 tex = tex2D(_MainTex, _MainTex_ST.zw * input.tex.xy + _MainTex_ST.xy);
				//return _Color;

				return tex * _ColorTint;
			}

			ENDCG
		}
	}
	//FallBack "Diffuse"
}
